﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;

namespace TWD
{
    /// <summary>
    /// Interaction logic for GuidePage.xaml
    /// </summary>
    public partial class GuidePage : Page
    {
        public GuidePage()
        {
            InitializeComponent();
        }
        private void Page_Loaded(object sender, RoutedEventArgs e)
        {
            imgMap.Source = new BitmapImage(new Uri(@"/Resources/Maps/stage1.png", UriKind.Relative));
            tbxContent.Text = Scenario.LoadBackground(1);
            tbxSetting.Text = Scenario.LoadBackground(2);
            tbxWarning.Text = Scenario.LoadBackground(3);

        }
    }
}
