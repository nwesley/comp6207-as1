﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;

namespace TWD
{
    /// <summary>
    /// Interaction logic for SettingPage.xaml
    /// </summary>
    public partial class SettingPage : Page
    {
        public SettingPage()
        {
            InitializeComponent();
        }
        private void Page_Loaded(object sender, RoutedEventArgs e)
        {
            imgMap.Source = new BitmapImage(new Uri(@"/Resources/Maps/stage1.png", UriKind.Relative));
        }

        private void chbxAllAudio_CheckedChanged(object sender, RoutedEventArgs e)
        {
            //MessageBox.Show("All Checked Changed");
            bool newVal = (chbxAllAudio.IsChecked == true);
            chbxMasterAudio.IsChecked = newVal;
            chbxMusic.IsChecked = newVal;
            chbxSoundEffects.IsChecked = newVal;
            chbxVoice.IsChecked = newVal;
        }

        private void chbxAudio_CheckedChanged(object sender, RoutedEventArgs e)
        {
            //MessageBox.Show("One Checked Change");
            chbxAllAudio.IsChecked = null;
            if((chbxMasterAudio.IsChecked==true)&&(chbxMusic.IsChecked==true)&&(chbxSoundEffects.IsChecked==true)&&(chbxVoice.IsChecked==true))
            {
                chbxAllAudio.IsChecked = true;
            }
            if ((chbxMasterAudio.IsChecked == false) && (chbxMusic.IsChecked == false) && (chbxSoundEffects.IsChecked == false) && (chbxVoice.IsChecked == false))
            {
                chbxAllAudio.IsChecked = false;
            }
        }
    }
}
