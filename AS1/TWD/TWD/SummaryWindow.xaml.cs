﻿using System;
using System.Windows;

namespace TWD
{
    /// <summary>
    /// Interaction logic for SummaryWindow.xaml
    /// </summary>
    public partial class SummaryWindow : Window
    {
        public static string page;

        public SummaryWindow()
        {
            InitializeComponent();
        }

        public void FailedGame(string playerproperty, int result)
        {
            switch (playerproperty)
            {
                case "playermorale":
                    page = "SuicidePage";
                    DeterminePage(page);
                    break;

                case "groupmorale":
                    page = "MurderedPage";
                    DeterminePage(page);
                    break;

                default:
                    MessageBox.Show("Not a valid player property");
                    break;

            }
        }
        public void FinishedGame(string value)
        {
            if(value == "success")
            {
                page = "SuccessPage";
                DeterminePage(page);
            }
        }
        public void DeterminePage(string page)
        {
            frmeGamePage.Source = new Uri(page + ".xaml", UriKind.Relative);
        }
        private void btnRestart_Click(object sender, RoutedEventArgs e)
        {
            ScenarioWindow scenario = new ScenarioWindow();
            scenario.Show();
            scenario.RestartGame();
            this.Close();
        }
        private void btnFinish_Click(object sender, RoutedEventArgs e)
        {
            MainWindow mainwindow = new MainWindow();
            mainwindow.Show();
            this.Close();
        }
    }
}
