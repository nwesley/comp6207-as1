﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Threading;
using Microsoft.Win32;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Media.Animation;
using System.Windows.Navigation;
using System.Windows.Shapes;
using System.ComponentModel;

namespace TWD
{
    /// <summary>
    /// Interaction logic for BackgroundScenario.xaml
    /// </summary>
    public partial class BackgroundScenario : Window, INotifyPropertyChanged
    {
        public static DoubleAnimation xslow = new DoubleAnimation(0.8, TimeSpan.FromSeconds(5));
        public static DoubleAnimation vanish = new DoubleAnimation();
        public static DoubleAnimation appear = new DoubleAnimation();
        public static int stageIndex = 1;
        public static int txtIndex = 1;
        public static string[] maps = new string[] { "stage1", "stage2", "stage3", "stage4", "stage5", "stage6", "stage7", "stage8", "stage9" };
        public static ScenarioWindow IndexMainWindow;
        private DispatcherTimer dispatcherTimer = new DispatcherTimer();
        private DispatcherTimer textTimer = new DispatcherTimer();


        public BackgroundScenario()
        {
            InitializeComponent();
            Scenario.BackgroundWindow = this;
            dispatcherTimer.Tick += new EventHandler(dispatcherTimer_Tick);
            dispatcherTimer.Interval = new TimeSpan(0, 0, 6);
            dispatcherTimer.Start();
            textTimer.Tick += new EventHandler(textTimer_Tick);
            textTimer.Interval = new TimeSpan(0, 0, 18);
            textTimer.Start();

        }
        public event PropertyChangedEventHandler PropertyChanged;
        protected virtual void OnPropertyChanged(string propertyName)
        {
            PropertyChangedEventHandler e = PropertyChanged;
            if (e != null)
            {
                e(this, new PropertyChangedEventArgs(propertyName));
            }
        }
        private int counter;
        public int Counter
        {
            get { return counter; }
            set
            {
                counter = value;
                OnPropertyChanged("Counter");
            }
        }
        private int txtCounter;
        public int TxtCounter
        {
            get { return txtCounter; }
            set
            {
                txtCounter = value;
                OnPropertyChanged("TxtCounter");
            }
        }
        private void Window_Loaded(object sender, RoutedEventArgs e)
        {
            imgBio.Opacity = 0;
            tbxTitle.Opacity = 0;
            tbxContent.Opacity = 0;
            imgBio.Source = new BitmapImage(new Uri(@"/Resources/Icons/biohazard.png", UriKind.Relative));
            imgMap.Source = new BitmapImage(new Uri(@"/Resources/Maps/stage1.png", UriKind.Relative));
            imgMap.Opacity = 0;
            imgMap.BeginAnimation(Image.OpacityProperty, xslow);
            tbxContent.Text = Scenario.LoadBackground(1);
            tbxTitle.BeginAnimation(TextBox.OpacityProperty, xslow);
            tbxContent.BeginAnimation(TextBox.OpacityProperty, xslow);
            btnPlay.Click += btnPlay_Click;
            btnBack.Click += btnBack_Click;
           

        }
        //Method linked with dispatcher timer to control how long each image is displayed. 
        private void textTimer_Tick(object sender, EventArgs e)
        {
            TxtCounter++;
            if (TxtCounter > 2)
            {
                //If counter is more than 2, resets variables to continue loop of images
                dispatcherTimer = null;
                txtIndex = 0;
                TxtCounter = 0;
            }
            txtIndex++;
            ChangeTextboxContent(txtIndex);
        }
        public void ChangeTextboxContent(int txtIndex)
        {
            tbxContent.Text = Scenario.LoadBackground(txtIndex);
            FadeTextAnimation();

        }
        //Method linked with dispatcher timer to control how long each image is displayed. 
        private void dispatcherTimer_Tick(object sender, EventArgs e)
        {
            Counter++;
            if (Counter > 9)
            {
                //If counter is more than 9, resets variables to continue loop of images
                //dispatcherTimer = null;
                stageIndex = 0;
                Counter = 0;
            }
            else if (Counter==8)
            {
                ImageFadeAnimation();
            }
            else if(Counter==7)
            {
                ImageScaleAnimation();
            }
            stageIndex++;
            ChangeMapImage(stageIndex);
        }
        //Method to change the image to the next one in the maps array
        public void ChangeMapImage(int stageIndex)
        {
            string filepath = "/Resources/Maps/stage" + stageIndex.ToString() + ".png";
            imgMap.Source = new BitmapImage(new Uri(@filepath, UriKind.Relative));
            AppearAnimation();
        }

        private void btnPlay_Click(object sender, RoutedEventArgs e)
        {
            //Biohazard Image Popup Here
            //dispatcherTimer.Stop();
            //textTimer.Stop();
            ScenarioWindow wnd = new ScenarioWindow();
            wnd.Show();
            this.Close();
        }
        private void btnBack_Click(object sender, RoutedEventArgs e)
        {
            MainWindow m = new MainWindow();
            m.Show();
            this.Close();
        }

        //Method to combine animations to storyboard in between image transitions
        public void AppearAnimation()
        {
            imgMap.Name = "imgMap";
            this.RegisterName(imgMap.Name, imgMap);
            Storyboard a = new Storyboard();
            appear.From = 0.20;
            appear.To = 0.80;
            appear.Duration = new Duration(TimeSpan.FromMilliseconds(5000));
            Storyboard.SetTargetName(appear, imgMap.Name);
            Storyboard.SetTargetProperty(appear, new PropertyPath(Image.OpacityProperty));
            a.Children.Add(appear);
            a.Begin(imgMap);
        }

        //Method to combine animations to storyboard in between image transitions
        public void FadeTextAnimation()
        {
            tbxContent.Name = "txtbxContent";
            this.RegisterName(tbxContent.Name, tbxContent);
            Storyboard tbx = new Storyboard();
            appear.From = 0;
            appear.To = 1;
            appear.Duration = new Duration(TimeSpan.FromMilliseconds(1000));
            Storyboard.SetTargetName(appear, tbxContent.Name);
            Storyboard.SetTargetProperty(appear, new PropertyPath(TextBlock.OpacityProperty));
            tbx.Children.Add(appear);
            tbx.Begin(tbxContent);
        }

        public void ImageScaleAnimation()
        {
            var translate_x = new DoubleAnimation(){ From = 0, To = -50, Duration = TimeSpan.FromSeconds(8)};
            var translate_y = new DoubleAnimation(){ From = 0, To = -50, Duration = TimeSpan.FromSeconds(8)};
            var increaseScale_x = new DoubleAnimation(){ From = 1, To = 2, Duration = TimeSpan.FromSeconds(8)};
            var increaseScale_y = new DoubleAnimation(){ From = 1, To = 2, Duration = TimeSpan.FromSeconds(8)};
            var imgOpacity = new DoubleAnimation(){From = 0, To = 1, Duration = TimeSpan.FromSeconds(8)};
            imgBio.BeginAnimation(Image.OpacityProperty, imgOpacity);
            imgScale.BeginAnimation(ScaleTransform.ScaleXProperty, increaseScale_x);
            imgScale.BeginAnimation(ScaleTransform.ScaleYProperty, increaseScale_y);
            imgTrans.BeginAnimation(TranslateTransform.XProperty, translate_x);
            imgTrans.BeginAnimation(TranslateTransform.YProperty, translate_y);
        }

        public void ImageFadeAnimation()
        {
            imgBio.Opacity = 100;
            var imgFadeOpacity = new DoubleAnimation() { From = 1, To = 0, Duration = TimeSpan.FromSeconds(9)};
            imgBio.BeginAnimation(Image.OpacityProperty, imgFadeOpacity);
        }

        private void tbxTitle_SelectionChanged(object sender, RoutedEventArgs e)
        {
            //Textbox Title empty eventhandler for selection changed
        }
    }
}
