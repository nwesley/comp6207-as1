﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;

namespace TWD
{
    /// <summary>
    /// Methods to detect resource text file folder locations, read the text file names, and to take the content from the text files
    /// to then be stored within application. 
    /// </summary>
    public class Scenario
    {
		/// <summary>
		/// All information of scenario is stored into this field 'Scenarios'.
		/// Key  : Scenario Code
		/// Value: Scenario Contents (This is string ARRAY which is contained the contents of scenario)
		/// </summary>
		public static SortedDictionary<string, string[]> Scenarios = new SortedDictionary<string, string[]>();


		/// <summary>
		/// When you load all scenario, you can call this method.
		/// And you can understand what process is going by reading the methods in this method Load().
		/// </summary>
		public static void Load()
		{
			// 1. 
			SortedDictionary<string, string> fileAndFolderNames;
			fileAndFolderNames = LoadFolderAndFileNames();

			// 2. 
			SetScenarios(fileAndFolderNames);
		}

		/// <summary>
		/// Loads folder and file names from LoadData.txt file.
		/// </summary>
		/// <returns></returns>
        private static SortedDictionary<string, string> LoadFolderAndFileNames()
        {
			// Key  : File name	<= Scenario Code
			// Value: Folder name
			SortedDictionary<string, string> fileAndFolderNames = new SortedDictionary<string, string>();

			// Data will be stored like the following
			// ----------------------------------------------
			// loadData[0] = "S1|S1,S1OP1,S1OP2,S1OP3,S1OP4"
			// loadData[0] = "S2|S2OP1,S2OP2,S2OP3,S2OP4"
			// loadData[0] = "S3|S3OP1,S3OP2,S3OP3,S3OP4"
			// ----------------------------------------------
			string[] loadData = System.IO.File.ReadAllLines(@"Resources\LoadData.txt");

			string[] aFolderAndFileNames;
			string aFolderName;
			string[] fileNames;

			foreach (string item in loadData)
            {
				// ----------------------------------------------
				// Data will be stored like the following
				// ----------------------------------------------
				// aFolderAndFilesName[0] = "S1"
				// aFolderAndFilesName[1] = "S1,S1OP1,S1OP2,S1OP3,S1OP4"
				// ----------------------------------------------
				aFolderAndFileNames = item.Split('|');
				aFolderName = aFolderAndFileNames[0];
				fileNames = aFolderAndFileNames[1].Split(',');

				// ----------------------------------------------
				// Data will be stored like the following
				// ----------------------------------------------
				// (key=fileName, value=folderName)
				// ("S1", "S1")
				// ("S1OP1", "S1")
				// ("S1OP2", "S1")
				// ("S1OP3", "S1")
				// ("S1OP4", "S1")
				foreach (string aFileName in fileNames)
				{
					fileAndFolderNames.Add(aFileName, aFolderName);
				}
            }

			return fileAndFolderNames;
        }

		/// <summary>
		/// Sets Scenarios information, which are codes and contents, into the field Scenarios.
		/// </summary>
		/// <param name="foldersAndFilesName"></param>
        private static void SetScenarios(SortedDictionary<string, string> foldersAndFilesName)
        {
			string aFolderName, aFileName, scenarioCode;
			string[] contents;

			// Key  : File name	<= Scenario Code
			// Value: Folder name
			foreach (KeyValuePair<string, string> kvp in foldersAndFilesName)
            {
                aFolderName = kvp.Value;
                aFileName = scenarioCode = kvp.Key;
                contents = System.IO.File.ReadAllLines(@"Resources\" + aFolderName + @"\" + aFileName + ".txt");

				Scenarios.Add(scenarioCode, contents);
            }
        }

		/// <summary>
		/// Use this method to display or test.
		/// </summary>
		/// <param name="code"></param>
		/// <param name="contentNumber"></param>
		public static void Display(string code, int contentNumber)
		{
			Console.WriteLine("Scenario Code is '" + code + "'");
			Console.Write("Scenario Content[" + contentNumber + "] is =>> ");

			// -----------------------------------------------------------------
			// Console.WriteLine(((string[])Scenarios[code])[contentNumber]);
			// -----------------------------------------------------------------
			// This above statement is the same as the following
			//
			//string[] contents = (string[])Scenarios[code];
			//Console.WriteLine(contents[contentNumber]);
			Console.WriteLine(((string[])Scenarios[code])[contentNumber]);
		}
    }
}
