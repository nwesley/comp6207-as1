﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;

namespace TWD
{
    /// <summary>
    /// Interaction logic for HelpPage.xaml
    /// </summary>
    public partial class HelpPage : Page
    {
        public HelpPage()
        {
            InitializeComponent();
        }
        private void Page_Loaded(object sender, RoutedEventArgs e)
        {
            imgMap.Source = new BitmapImage(new Uri(@"/Resources/Maps/stage1.png", UriKind.Relative));
            txtRandom.Text= "Lorem ipsum dolor sit amet, consectetur adipiscing elit. Duis lobortis iaculis vulputate. In mattis efficitur ligula id consectetur. Donec nisl nisl, varius vitae ligula eget, faucibus efficitur nulla. Vestibulum malesuada tellus et ligula eleifend, accumsan aliquet libero tempus. Morbi at metus scelerisque, suscipit ex vel, dignissim mauris. Orci varius natoque penatibus et magnis dis parturient montes, nascetur ridiculus mus. In venenatis nisl elementum neque auctor cursus. Donec ac mollis dolor. Etiam elementum augue mauris, vel accumsan quam facilisis in. Vivamus blandit odio sit amet ipsum porttitor fermentum sit amet quis enim. Cras elementum fringilla vulputate. Proin eu nunc nisi. Lorem ipsum dolor sit amet, consectetur adipiscing elit.Duis lobortis iaculis vulputate. In mattis efficitur ligula id consectetur.";
        }
        private void TextBox_SelectionChanged(object sender, RoutedEventArgs e)
        {
            TextBox textBox = sender as TextBox;
            txtStatus.Text = "Selection starts at character #" + textBox.SelectionStart + Environment.NewLine;
            txtStatus.Text += "Selection is " + textBox.SelectionLength + " character(s) long" + Environment.NewLine;
            txtStatus.Text += "Selected text: '" + textBox.SelectedText + "'";
        }
    }
}
