Content[:]Inform everyone that food will be rationed until the culprit is found
Consequence[:]Jason confessed to taking the missing food and the group has decided to ration his next meals. The group approves of approaching the issue with everyone involved.
PlayerMorale[:]+10
GroupMorale[:]+10
Perk[:]null