Content[:]Keep your farm's location safe and kill the stranger
Consequence[:]By killing the stranger, you've ensured that your camp's location remains undetected. Jason and Michael approve of your decision, but you don't feel good about her death.
PlayerMorale[:]-30
GroupMorale[:]+20
Perk[:]Farm Undetected