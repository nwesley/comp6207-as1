Content[:]Search the nearest town for food and supplies
Consequence[:]The trip into town cost fuel but you found plenty of food and supplies in return. Your group approves of your decision.
PlayerMorale[:]+20
GroupMorale[:]+20
Perk[:]null