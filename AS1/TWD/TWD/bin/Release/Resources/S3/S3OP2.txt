Content[:]Tell the stranger that you don't have enough food and let her leave
Consequence[:]The stranger leaves your group's camp without food. You're not sure whether she will keep her promise to never return. Michael is the only person to disapprove of your decision as your farm's location is no longer safe.
PlayerMorale[:]+0
GroupMorale[:]+10
Perk[:]Farm Detected