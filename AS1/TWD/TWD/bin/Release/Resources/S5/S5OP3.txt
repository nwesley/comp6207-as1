Content[:]Allow the group to kill and eat the farm's cow. 
Consequence[:]By killing your only cow, you've supplied your group with plenty of fresh meat. Your group approves of your decision. 
PlayerMorale[:]+40
GroupMorale[:]+40
Perk[:]null