﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Animation;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;

namespace TWD
{
    /// <summary>
    /// Interaction logic for ScenarioWindow.xaml
    /// </summary>
    public partial class ScenarioWindow : Window
    {
        public static DoubleAnimation brighten = new DoubleAnimation(1, TimeSpan.FromSeconds(0.5));
        public static DoubleAnimation darken = new DoubleAnimation(0.6, TimeSpan.FromSeconds(0.5));
        public static DoubleAnimation vanish = new DoubleAnimation(0, TimeSpan.FromSeconds(0.5));
        public static DoubleAnimation slow = new DoubleAnimation(1, TimeSpan.FromSeconds(2));
        public static DoubleAnimation fast = new DoubleAnimation(1, TimeSpan.FromSeconds(1.5));

        public String scenarioCode;
        public ScenarioWindow()
        {
            InitializeComponent();
            Scenario.IndexMainWindow = this;       
        }
        private void Window_Loaded(object sender, RoutedEventArgs e)
        {
            if(Scenario.Scenarios.Count==0)
            {
                Scenario.LoadScenario();
            }
            Player.ResetPlayerProperties();
            DisplayScene("S1");
        }

        public void DisplayScene(string code)
        {
            FadeAnimation();
            string scene = code;
            lblPlayerStatus.Content = "Player Status : " + Player.PlayerStatus;
            lblPlayerMorale.Content = Player.PlayerMorale.ToString() + "%";
            lblGroupMorale.Content = Player.GroupMorale.ToString() + "%";
            imgScene.Source = new BitmapImage(new Uri(@"/Resources/Icons/scenes/" + scene + ".png", UriKind.Relative));
            imgRedMap.Source = new BitmapImage(new Uri(@"/Resources/Maps/stage9.png", UriKind.Relative));
            txtbxScenarioContent.Text = (string)Scenario.GetScenarioContent(code, "scontent");
            lblScenarioQuestion.Content = (string)Scenario.GetScenarioContent(code, "question");
            string op1 = (string)Scenario.GetScenarioContent(scene + "OP1", "content");
            string op2 = (string)Scenario.GetScenarioContent(scene + "OP2", "content");
            string op3 = (string)Scenario.GetScenarioContent(scene + "OP3", "content");
            string op4 = (string)Scenario.GetScenarioContent(scene + "OP4", "content");
            lbScenarioOptions.Items.Add(new ListBoxItem("OP1", op1, scene));
            lbScenarioOptions.Items.Add(new ListBoxItem("OP2", op2, scene));
            lbScenarioOptions.Items.Add(new ListBoxItem("OP3", op3, scene));
            lbScenarioOptions.Items.Add(new ListBoxItem("OP4", op4, scene));

            string playerFilepath = Player.UpdateStatusImage(Player.GroupMorale);
            imgPstatus.Source = new BitmapImage(new Uri(@"/Resources/Icons/player/" + playerFilepath, UriKind.Relative));

            string groupFilepath = Player.UpdateStatusImage(Player.GroupMorale);
            imgGstatus.Source = new BitmapImage(new Uri(@"/Resources/Icons/group/" + groupFilepath, UriKind.Relative));
        }
        public void LbScenarioOptions_SelectionChanged(object sender, SelectionChangedEventArgs e)
        {
            // (After clearing listbox items) This will return if their are no items in the listbox
            if (lbScenarioOptions.Items.Count == 0)
            {
                return;
            }
            ListBoxItem userSelection = (ListBoxItem)lbScenarioOptions.SelectedItem;
            string scene = userSelection.Scene;
            string code = userSelection.Code;

            //Retrieves the value consequence of the selected option for PLAYER MORALE, 
            //      Updates the group status feeling using the playerResult value 
            //          & Updates the group status image using the updaded playerMorale value
            int playerResult = Scenario.GetOptionValue(scene, code, "playermorale");
            Player.PlayerMorale = Player.UpdatePlayerStatus("playermorale", playerResult);
            if (Player.PlayerMorale == 0)
                this.Close();
            string playerFilepath = Player.UpdateStatusImage(Player.PlayerMorale);
            imgPstatus.Source = new BitmapImage(new Uri(@"/Resources/Icons/player/" + playerFilepath, UriKind.Relative));

            //Retrieves the value consequence of the selected option for GROUP MORALE, 
            //      Updates the group status feeling using the groupResult value 
            //          & Updates the group status image using the updaded GroupMorale value
            int groupResult = Scenario.GetOptionValue(scene, code, "groupmorale");
            Player.GroupMorale = Player.UpdatePlayerStatus("groupmorale", groupResult);
            if (Player.GroupMorale == 0)
                this.Close();
            string groupFilepath = Player.UpdateStatusImage(Player.GroupMorale);
            imgGstatus.Source = new BitmapImage(new Uri(@"/Resources/Icons/group/" + groupFilepath, UriKind.Relative));

            //Fill out the scenario window with content
            Scenario.DisplayOptionSelection(scene, code);
            lblPlayerMorale.Content = Player.PlayerMorale.ToString() + "%";
            lblGroupMorale.Content = Player.GroupMorale.ToString() + "%";
            lblPlayerStatus.Content = "Player Status : " + Player.PlayerStatus; 
            NextScenario(scene);

        }
        public void ClearScenarioWindow()
        {
            txtbxScenarioContent.Text = String.Empty;
            lblScenarioQuestion.Content = String.Empty;
            imgGstatus.Source = null;
            imgPstatus.Source = null;
            lblPlayerStatus.Content = String.Empty;
            lblPlayerMorale.Content = String.Empty;
            lblGroupMorale.Content = String.Empty;
            //LbScenarioOptions_SelectionChanged is triggered when listbox items are cleared.
            lbScenarioOptions.Items.Clear();
        }
        public void FadeAnimation()
        {
            //All content starts out transparent
            lblGroupMorale.Opacity = 0;
            lblPlayerMorale.Opacity = 0;
            lblPlayerStatus.Opacity = 0;
            imgPstatus.Opacity = 0;
            imgGstatus.Opacity = 0;
            txtbxScenarioContent.Opacity = 0;
            lblScenarioQuestion.Opacity = 0;
            lbScenarioOptions.Opacity = 0;
            imgScene.Opacity = 0;

            //All content fades brighter
            imgPstatus.BeginAnimation(Image.OpacityProperty, slow);
            imgGstatus.BeginAnimation(Image.OpacityProperty, slow);
            lblScenarioQuestion.BeginAnimation(Image.OpacityProperty, slow);
            txtbxScenarioContent.BeginAnimation(Image.OpacityProperty, slow);
            lblPlayerStatus.BeginAnimation(Image.OpacityProperty, slow);
            lblGroupMorale.BeginAnimation(Image.OpacityProperty, slow);
            lblPlayerMorale.BeginAnimation(Image.OpacityProperty, slow);
            lbScenarioOptions.BeginAnimation(Image.OpacityProperty, slow);
            imgScene.BeginAnimation(Image.OpacityProperty, slow);

        }
        public void NextScenario(string scene)
        {
            switch(scene)
            {
                case "S1":
                    {
                        ClearScenarioWindow();
                        DisplayScene("S2");
                    }
                    break;
                case "S2":
                    {
                        ClearScenarioWindow();
                        DisplayScene("S3");
                    }
                    break;
                case "S3":
                    {
                        ClearScenarioWindow();
                        DisplayScene("S4");
                    }
                    break;
                case "S4":
                    {
                        ClearScenarioWindow();
                        DisplayScene("S5");
                    }
                    break;
                case "S5":
                    {
                        ClearScenarioWindow();
                        DisplayScene("S6");
                    }
                    break;
                case "S6":
                    {
                        ClearScenarioWindow();
                        DisplayScene("S7");
                    }
                    break;
                case "S7":
                    {
                        ClearScenarioWindow();
                        DisplayScene("S8");
                    }
                    break;
                case "S8":
                    {
                        ClearScenarioWindow();
                        DisplayScene("S9");
                    }
                    break;
                case "S9":
                    {
                        string s = "success";
                        SummaryWindow swindow = new SummaryWindow();
                        swindow.Show();
                        swindow.FinishedGame(s);
                        this.Close();
                    }
                    break;
                default:
                    {
                        MessageBox.Show("Scene (" + scene + ") not valid");
                        this.Close();
                    }
                    break;

            }
        }

        private void btnRestart_Click(object sender, RoutedEventArgs e)
        {
            RestartGame();
        }
        public void RestartGame()
        {
            ClearScenarioWindow();
            Player.ResetPlayerProperties();
            DisplayScene("S1");
        }
    }
    public class ListBoxItem
    {
        public string Code;
        public string Content;
        public string Scene;

        public ListBoxItem(string code, string content, string scene)
        {
            Code = code;
            Content = content;
            Scene = scene;
        }

        public override string ToString()
        {
            return Content;
        }
    }
}